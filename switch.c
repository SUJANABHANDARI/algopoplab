#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: switch.alg		*/
/* Compiled at Thu Jul 12 13:17:42 2018		*/


#include	<stdio.h>
#include "switch.h"

//	Code for the global declarations

jmp_buf _March_40;
jmp_buf _Feb_40;
jmp_buf _Jan_40;
int _i_40; /* i declared at line 2*/
 /* month declared at line 3*/
jmp_buf *_month_40 (char *LP, int n) {
 switch (n) {
default:
case 1: return _Jan_40;
case 2: return _Feb_40;
case 3: return _March_40;

}; // end of switch list 
} 
int  A_jff_0A (char *LP, int V){
return _i_40 = V;
}
int  _jff_0A (char *LP, int d){
return _i_40;
}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
if (setjmp (_March_40)) goto L_March_40;
if (setjmp (_Feb_40)) goto L_Feb_40;
if (setjmp (_Jan_40)) goto L_Jan_40;
outstring (1, "Which month");
ininteger (0, LP, A_jff_0A, _jff_0A);
__jff_longjmp (_month_40 (LP, _i_40));
L_Jan_40:;outstring (1, "Jan has 31 days");
goto L_endlabel_40;
L_Feb_40:;outstring (1, "Jan has 31 days");
goto L_endlabel_40;
L_March_40:;outstring (1, "Jan has 31 days");
goto L_endlabel_40;
L_endlabel_40:;}
}
