#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: passbyref.alg		*/
/* Compiled at Thu Jul 12 13:01:12 2018		*/


#include	<stdio.h>
#include "passbyref.h"

//	Code for the global declarations

int _n_40; /* n declared at line 2*/
 /* p declared at line 3*/
void _p_41 (int k){ 
 outinteger (1, k);
_n_40=(_n_40) + (1);
outinteger (1, k);

}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
_n_40=0;
_p_41 ((_n_40) + (10));
}
}
