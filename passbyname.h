#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: passbyname.alg		*/
/* Compiled at Thu Jul 12 13:03:41 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void outinteger (int,int); /* outinteger declared at line 40*/
extern int _n_40; /* n declared at line 2*/
extern	void _p_41 (char	*,int (*)(char *, int), int(*)(char *, int)); /* p declared at line 3*/
struct ___p_41_rec {
char *__l_field;
char	*Lk;
 int (*Ak)(char *, int);
int (*Vk)(char *, int);
};

//	specification for thunk
extern int A_jff_0A (char *, int);
extern int _jff_0A (char *, int);
