#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: passbyname.alg		*/
/* Compiled at Thu Jul 12 13:03:41 2018		*/


#include	<stdio.h>
#include "passbyname.h"

//	Code for the global declarations

int _n_40; /* n declared at line 2*/
 /* p declared at line 3*/
void _p_41 (char	*Lk, int (*Ak)( char *, int), int (*Vk)(char *, int)){ 
 struct ___p_41_rec local_data_p;
struct ___p_41_rec *LP = & local_data_p;
LP -> Lk = Lk;
LP -> Ak = Ak;
LP -> Vk = Vk;
_n_40=(_n_40) + (1);
((LP) -> Ak) ((LP) -> Lk, (((LP) -> Vk)(((LP) -> Lk), 0)) + (4));
outinteger (1, _n_40);

}
int  A_jff_0A (char *LP, int V){
return _n_40 = V;
}
int  _jff_0A (char *LP, int d){
return _n_40;
}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
_p_41 (LP, A_jff_0A, _jff_0A);
outinteger (1, _n_40);
}
}
