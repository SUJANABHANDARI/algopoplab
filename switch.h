#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: switch.alg		*/
/* Compiled at Thu Jul 12 13:17:42 2018		*/


#include	<stdio.h>
#include "/usr/local/include/jff_header.h"
/* Headers		*/
extern	void outstring (int,char	*); /* outstring declared at line 26*/
extern	void ininteger (int,char	*,int (*)(char *, int), int(*)(char *, int)); /* ininteger declared at line 45*/
extern	jmp_buf _March_40; /* March declared at line 14*/
extern	jmp_buf _Feb_40; /* Feb declared at line 12*/
extern	jmp_buf _Jan_40; /* Jan declared at line 9*/
extern int _i_40; /* i declared at line 2*/
jmp_buf	*_month_40 (char *, int);	 /* month declared at line 3*/

//	specification for thunk
extern int A_jff_0A (char *, int);
extern int _jff_0A (char *, int);
