#

/* jff_algol (Jan van Katwijk) */
/* Version 2.1.1		*/
/* input file: passbyvalue.alg		*/
/* Compiled at Thu Jul 12 13:03:54 2018		*/


#include	<stdio.h>
#include "passbyvalue.h"

//	Code for the global declarations

int _n_40; /* n declared at line 2*/
 /* p declared at line 3*/
void _p_41 (int k){ 
 _n_40=(_n_40) + (1);
k=(k) + (4);
outinteger (1, _n_40);

}


// The main program
int	main () {
 char	*LP = (char *)NULL;

{ // code for block at line 2
_p_41 (_n_40);
outinteger (1, _n_40);
}
}
